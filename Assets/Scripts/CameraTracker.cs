﻿using UnityEngine;

public class CameraTracker : MonoBehaviour
{
    [SerializeField] private PlayerTower _playerTower;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private Vector3 _offsetPosition;
    [SerializeField] private Vector3 _offsetRotation;

    private Vector3 _targetOffsetPosition;

    private void OnEnable()
    {
        _playerTower.HumanAdded += OnHumanAdded;
    }

    private void OnDisable()
    {
        _playerTower.HumanAdded -= OnHumanAdded;
    }

    private void Update()
    {
        UpdatePosition();
        _offsetPosition = Vector3.MoveTowards(_offsetPosition, _targetOffsetPosition, _moveSpeed * Time.deltaTime);
    }

    private void UpdatePosition()
    {
        transform.position = _playerTower.transform.position;
        transform.localPosition += _offsetPosition;

        Vector3 lookAtPoint = _playerTower.transform.position + _offsetRotation;

        transform.LookAt(lookAtPoint);
    }

    private void OnHumanAdded(int amount)
    {
        float offsetUpRatio = 0.1f;
        float offsetBackRatio = 0.1f;

        Vector3 offsetUp = Vector3.up * offsetUpRatio;
        Vector3 offsetBack = Vector3.back * offsetBackRatio;

        _targetOffsetPosition = _offsetPosition + (offsetUp + offsetBack) * amount;
        UpdatePosition();
    }
}

﻿using UnityEngine;

[RequireComponent(typeof(Animator))]

public class Human : MonoBehaviour
{
    [SerializeField] private Transform _fixationPoint;

    private Animator _animator;

    public Transform FixationPoint
    {
        get => _fixationPoint;
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Run()
    {
        _animator.SetBool("isRunning", true);
    }

    public void StopRun()
    {
        _animator.SetBool("isRunning", false);
    }

    public void StartTexting()
    {
        _animator.SetBool("isTexting", true);
    }

    public void StopTexting()
    {
        _animator.SetBool("isTexting", false);
    }
}

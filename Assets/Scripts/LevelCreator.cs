﻿using PathCreation;
using UnityEngine;

public class LevelCreator : MonoBehaviour
{
    [SerializeField] private PathCreator _pathCreator;
    [SerializeField] private HumanTower _towerPrefab;
    [SerializeField] private int _towerAmount;

    private void Start()
    {
        GenerateLevel();
    }

    private void GenerateLevel()
    {
        float roadLength = _pathCreator.path.length;
        float distanceBetweenTowers = roadLength / _towerAmount;
        float nextSpawnDistance = 0;
        Vector3 spawnPoint;

        for (int i = 0; i < _towerAmount; i++)
        {
            nextSpawnDistance += distanceBetweenTowers;
            spawnPoint = _pathCreator.path.GetPointAtDistance(nextSpawnDistance, EndOfPathInstruction.Stop);

            Instantiate(_towerPrefab, spawnPoint, Quaternion.identity);
        }
    }
}

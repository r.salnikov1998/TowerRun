﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class HumanTower : MonoBehaviour
{
    [SerializeField] private Vector2Int _humanAmountRange;
    [SerializeField] private Human[] _humansPrefabs;

    private List<Human> _humansInTower;

    private void Start()
    {
        _humansInTower = new List<Human>();
        int humanAmountInTower = Random.Range(_humanAmountRange.x, _humanAmountRange.y);
        SpawnHumans(humanAmountInTower);
    }

    private void SpawnHumans(int humanAmount)
    {
        Vector3 spawnPoint = transform.position;

        for (int i = 0; i < humanAmount; i++)
        {
            Human pickedHuman = _humansPrefabs[Random.Range(0, _humansPrefabs.Length)];
            Human spawnedHuman = Instantiate(pickedHuman, spawnPoint, Quaternion.identity, transform);
            _humansInTower.Add(spawnedHuman);
            spawnedHuman.StartTexting();

            _humansInTower[i].transform.localPosition = new Vector3(0, _humansInTower[i].transform.localPosition.y, 0);
            spawnPoint = _humansInTower[i].FixationPoint.position;
        }

    }

    public List<Human> CollectHumans(Transform distanceChecker, float fixationMaxDistance)
    {
        for (int i = 0; i < _humansInTower.Count; i++)
        {
            float distanceBetweenPoints = CheckDistanceY(distanceChecker, _humansInTower[i].FixationPoint.transform);
            Debug.Log(distanceBetweenPoints);

            if(distanceBetweenPoints < fixationMaxDistance)
            {
                List<Human> collectedHumans = _humansInTower.GetRange(0, i + 1);
                _humansInTower.RemoveRange(0, i + 1);
                return collectedHumans;
            }
        }

        return null;
    }

    private float CheckDistanceY(Transform distanceChecker, Transform humanFixationPoint)
    {
        Vector3 distanceCheckerY = new Vector3(0, distanceChecker.position.y, 0);
        Vector3 humanFixationPointY = new Vector3(0, humanFixationPoint.position.y, 0);

        return Vector3.Distance(distanceCheckerY, humanFixationPointY);
    }

    public void Break()
    {
        Destroy(gameObject);
    }
}
